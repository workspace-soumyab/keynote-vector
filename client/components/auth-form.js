import React from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {auth, forgotPass} from '../store'

import { SignUpForm, LoginForm, ForgotPassForm } from "./Forms";

/**
 * COMPONENT
 */
const AuthForm = props => {
  const { name, displayName, handleSubmit, error, history } = props

  return (
    <div>
      <div id="auth-form">
        { name === 'signup'
            ? <SignUpForm handleSubmit={handleSubmit} error={error} name={name} />
            : (name === 'forgotpass')
              ? <ForgotPassForm handleSubmit={handleSubmit} history={history} error={error} name={name} />
              : <LoginForm handleSubmit={handleSubmit} history={history} error={error} name={name} />
        }
        { name !== 'forgotpass' ? (
          <form method="get" action="/auth/google">
          <div className="google-oauth">
            <button type="submit" id="google-oauth-button">
              {displayName} with Google
            </button>
          </div>
        </form>
        ): null}

      </div>
    </div>
  )
}

/**
 * CONTAINER
 *   Note that we have two different sets of 'mapStateToProps' functions -
 *   one for Login, and one for Signup. However, they share the same 'mapDispatchToProps'
 *   function, and share the same Component. This is a good example of how we
 *   can stay DRY with interfaces that are very similar to each other!
 */
const mapLogin = state => {
  return {
    name: 'login',
    displayName: 'Login',
    error: state.user.error
  }
}

const mapSignup = state => {
  return {
    name: 'signup',
    displayName: 'Sign Up',
    error: state.user.error
  }
}

const mapForgotPass = state => {
  return {
    name: 'forgotpass',
    displayName: 'Forgot Password',
    error: state.user.error
  }
}

const mapDispatch = dispatch => {
  return {
    handleSubmit(values) {
      const formName = values.formName;

      if (formName === 'signup') {
        const firstName = values.firstName;
        const lastName = values.lastName;
        const email = values.email;
        const password = values.password;
        dispatch(auth(email, password, formName, firstName, lastName))
      } else if(formName === 'forgotpass') {
        dispatch(forgotPass(values.email))
      } else {
        const email = values.email;
        const password = values.password;
        dispatch(auth(email, password, formName))
      }
    }
  }
}

export const Login = connect(mapLogin, mapDispatch)(AuthForm)
export const Signup = connect(mapSignup, mapDispatch)(AuthForm)
export const ForgotPass = connect(mapForgotPass, mapDispatch)(AuthForm)

/**
 * PROP TYPES
 */
AuthForm.propTypes = {
  name: PropTypes.string.isRequired,
  displayName: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.object,
  history: PropTypes.object
}
