import React, { Fragment } from "react";
import { Formik, Form, Field } from "formik";
import PropTypes from "prop-types";

import * as Yup from "yup";

const initialValues = {
  email: '',
  formName: 'forgotpass'
}

const ForgotPassSchema = Yup.object().shape({
  email: Yup.string().trim('Please remove whitespace').email("Email must be valid").strict().required('Please enter email address')
});

const ForgotPassForm = (props) => {
  const { handleSubmit, history, error, name } = props;
  return (
    <Fragment>
      <Formik
      initialValues={initialValues}
      validationSchema={ForgotPassSchema}
      onSubmit={handleSubmit}>
        {({ errors, touched }) => {
          return (
            <Form>
              {error && error.response && name === 'forgotpass' ? <div className="errorMsg padding">{error.response.data}</div> : null}
              <div>
                <label htmlFor="email">
                  <h5 className="auth-form-text">Email Address</h5>
                </label>
                <Field
                  name="email"
                  type="text"
                  className="input-stretch"
                  />
                  {errors.email && touched.email ? <div className="errorMsg">{errors.email}</div> : null}
              </div>
              <div id="auth-button-center">
                <Field type="hidden" name="formName" />
                <button id="auth-button" type="submit">
                  Submit
                </button>
                <div>
                  <button id="forgot-button" type="button" onClick={() => history.push('/login')}>
                    Back to Login
                  </button>
                </div>
              </div>
            </Form>
          );
        }}

      </Formik>
    </Fragment>
  );
}

ForgotPassForm.propTypes = {
  handleSubmit : PropTypes.func.isRequired,
  history: PropTypes.object,
  error: PropTypes.object,
  name: PropTypes.string.isRequired
}

export default ForgotPassForm;
