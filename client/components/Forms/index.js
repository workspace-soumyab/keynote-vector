import SignUp from "./SignUpForm";
import Login from "./LoginForm";
import ForgotPass from "./ForgotPassForm";

export {
  SignUp as SignUpForm,
  Login as LoginForm,
  ForgotPass as ForgotPassForm
};
