/* eslint-disable complexity */
import { Field, Form, Formik } from "formik";
import PropTypes from "prop-types";
import React from "react";
import * as Yup from "yup";


const initialValues = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  formName: 'signup'
}

const signUpFormSchema = Yup.object().shape({
  firstName: Yup.string().trim('Please remove whitespace').strict().required('Please enter first name'),
  lastName: Yup.string().trim('Please remove whitespace').strict().required('Please enter last name'),
  email: Yup.string().trim('Please remove whitespace').email("Email must be valid").strict().required('Please enter email address'),
  password: Yup.string().trim('Please remove whitespace').min(5, 'Password must be minimum 5 characters').strict().required('Please enter password'),
});



const SignUpForm = (props) => {
  const { handleSubmit, error = null } = props;
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={signUpFormSchema}
      onSubmit={handleSubmit}>
      {({ errors, touched }) => {
        return (
          <Form>
            {error && error.response ? <div className="errorMsg padding">{error.response.data}</div> : null}
            <div className="signup-no-margin">
              <div>
                <label htmlFor="firstName">
                  <h5 className="auth-form-text">First Name</h5>
                </label>
                <Field
                  type="text"
                  name="firstName"
                  className="input-stretch"
                  autoComplete="off"
                />
                {errors.firstName && touched.firstName ? <div className="errorMsg">{errors.firstName}</div> : null}
              </div>
              <div>
                <label htmlFor="lastName">
                  <h5 className="auth-form-text">Last Name</h5>
                </label>
                <Field
                  type="text"
                  name="lastName"
                  className="input-stretch"
                  autoComplete="off"
                />
                {errors.lastName && touched.lastName ? <div className="errorMsg">{errors.lastName}</div> : null}
              </div>
              <div>
                <label htmlFor="email">
                  <h5 className="auth-form-text">Email</h5>
                </label>
                <Field
                  type="text"
                  name="email"
                  className="input-stretch"
                  autoComplete="off"
                />
                {errors.email && touched.email ? <div className="errorMsg">{errors.email}</div> : null}
              </div>
              <div>
                <label htmlFor="password">
                  <h5 className="auth-form-text">Password</h5>
                </label>
                <Field
                  type="password"
                  name="password"
                  className="input-stretch"
                  autoComplete="off"
                />
                {errors.password && touched.password ? <div className="errorMsg">{errors.password}</div> : null}
              </div>
              <div id="auth-button-center">
                <Field type="hidden" name="formName" />
                <button id="auth-button" type="submit">
                  Sign Up
                </button>
              </div>
            </div>
          </Form>
        );
      }
      }
    </Formik>
  );
}

SignUpForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.object
}

export default SignUpForm;
