import React, { Fragment } from "react";
import { Formik, Form, Field } from "formik";
import PropTypes from "prop-types";

import * as Yup from "yup";

const initialValues = {
  email: '',
  password: '',
  rememberMe: false,
  formName: 'login'
}

const LoginSchema = Yup.object().shape({
  email: Yup.string().trim('Please remove whitespace').email("Email must be valid").strict().required('Please enter email address'),
  password: Yup.string().trim('Please remove whitespace').strict().required('Please enter password'),
});

const LoginForm = (props) => {
  const { handleSubmit, history, error, name } = props;
  return (
    <Fragment>
      <Formik
      initialValues={initialValues}
      validationSchema={LoginSchema}
      onSubmit={handleSubmit}>
        {({ errors, touched }) => {
          return (
            <Form>
              {error && error.response && name === 'login' ? <div className="errorMsg padding">{error.response.data}</div> : null}
              <div>
                <label htmlFor="email">
                  <h5 className="auth-form-text">Email</h5>
                </label>
                <Field
                  name="email"
                  type="text"
                  className="input-stretch"
                  />
                  {errors.email && touched.email ? <div className="errorMsg">{errors.email}</div> : null}
              </div>
              <div>
                <label htmlFor="password">
                  <h5 className="auth-form-text">Password</h5>
                </label>
                <Field
                  name="password"
                  type="password"
                  className="input-stretch"
                  />
                  {errors.password && touched.password ? <div className="errorMsg">{errors.password}</div> : null}
              </div>
              <div>
                <label htmlFor="password">
                  <h5 className="auth-form-text">Remember Me</h5>
                </label>
                <Field
                  name="rememberMe"
                  type="checkbox"
                  className="input-stretch"
                  />
              </div>
              <div id="auth-button-center">
                <Field type="hidden" name="formName" />
                <button id="auth-button" type="submit">
                  Login
                </button>
                <div>
                  <button id="forgot-button" type="button" onClick={() => history.push('/forgot-password')}>
                    Forgot Password
                  </button>
                </div>
              </div>
            </Form>
          );
        }}

      </Formik>
    </Fragment>
  );
}

LoginForm.propTypes = {
  handleSubmit : PropTypes.func.isRequired,
  history: PropTypes.object,
  error: PropTypes.object,
  name: PropTypes.string.isRequired
}

export default LoginForm;
